with open('input.txt') as f:
    raw = map(int, f.read().split(','))

occurrences = {}
last = 0


for i, v in enumerate(raw):
    occurrences[v] = i+1
    last = v

nxt = 0  # if the last number in the init sequence does not occur for the first time, this needs modification
for i in range(i + 2, 30000000):
    if nxt in occurrences:
        tmp_nxt = i - occurrences[nxt]
        occurrences[nxt] = i
        nxt = tmp_nxt
    else:
        occurrences[nxt] = i
        nxt = 0
print(nxt)

