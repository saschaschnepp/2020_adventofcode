with open('input.txt') as f:
    card_pub, door_pub = tuple(map(int, f.read().split('\n')))

modu = 20201227


def loop(in_: int, subject_: int) -> int:
    return (in_ * subject_) % modu


card_rounds = door_rounds = 0
rounds = 1
num = 1
while True:
    num = loop(num, 7)
    if not rounds % 100_000:
        print(rounds)
    if num == card_pub:
        print(f'Card rounds: {rounds}')
        card_rounds = rounds
    if num == door_pub:  # it's enough to get one round counter but anyway...
        print(f'Door rounds: {rounds}')
        door_rounds = rounds
    if card_rounds and door_rounds:
        break
    rounds += 1

num = 1
for _ in range(card_rounds):
    num = loop(num, door_pub)
print(num)

# Not needed to do the second option. Just cross-checking.
num = 1
for _ in range(door_rounds):
    num = loop(num, card_pub)
print(num)
