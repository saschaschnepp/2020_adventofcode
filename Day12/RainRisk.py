import numpy as np
import math

with open('input.txt') as f:
    raw = list(f.read().split('\n'))


class Position:

    def __init__(self, data: list, orientation: str):
        self.data = data
        self.NS = 0
        self.EW = 0
        self.dirs = list(range(4))
        self.direction_to_idx = dict(zip(['N', 'E', 'S', 'W'], self.dirs))
        self.idx_to_direction = {v: k for k, v in self.direction_to_idx.items()}
        self.current_dir = self.direction_to_idx[orientation]

    def turn(self, inpt: str):
        inc = int(inpt[1:])/90
        if inpt[0] == 'L':
            inc *= -1
        self.current_dir = (self.current_dir + 4 + inc) % 4

    def update(self, inpt: str):
        if inpt[0] == 'F':
            inpt = self.idx_to_direction[self.current_dir] + inpt[1:]
        if inpt[0] == 'N':
            self.NS += int(inpt[1:])
        elif inpt[0] == 'S':
            self.NS -= int(inpt[1:])
        elif inpt[0] == 'E':
            self.EW += int(inpt[1:])
        elif inpt[0] == 'W':
            self.EW -= int(inpt[1:])
        elif inpt[0] in ('L', 'R'):
            self.turn(inpt)

    def manhattan_distance(self):
        print(abs(self.NS) + abs(self.EW))

    def process(self):
        for d in self.data:
            self.update(d)
        self.manhattan_distance()


class Waypoint:

    def __init__(self, data: list):
        self.data = data
        self.pos = np.asarray([0, 0])
        self.wp = np.asarray([10, 1])
        self.dirs = list(range(4))
        self.direction_to_idx = dict(zip(['N', 'E', 'S', 'W'], self.dirs))
        self.idx_to_direction = {v: k for k, v in self.direction_to_idx.items()}

    def rotate(self, inpt: str):

        angle = math.radians(int(inpt[1:]))
        if inpt[0] == 'R':
            angle = 2*np.pi - angle

        rot = np.array([
            [math.cos(angle), -math.sin(angle)],
            [math.sin(angle), math.cos(angle)]
        ])
        self.wp = np.rint(np.dot(rot, self.wp)).astype(int)

    def update(self, inpt: str):
        if inpt[0] == 'F':
            self.pos = self.pos + int(inpt[1:])*self.wp
        elif inpt[0] == 'N':
            self.wp[1] += int(inpt[1:])
        elif inpt[0] == 'S':
            self.wp[1] -= int(inpt[1:])
        elif inpt[0] == 'E':
            self.wp[0] += int(inpt[1:])
        elif inpt[0] == 'W':
            self.wp[0] -= int(inpt[1:])
        elif inpt[0] in ('L', 'R'):
            self.rotate(inpt)

    def manhattan_distance(self):
        print(abs(self.pos[0]) + abs(self.pos[1]))

    def process(self):
        for d in self.data:
            self.update(d)
        self.manhattan_distance()


if __name__ == '__main__':

    p = Position(raw, 'E')
    p.process()

    wp = Waypoint(raw)
    wp.process()
