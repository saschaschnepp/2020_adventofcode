from sympy.ntheory.modular import crt, solve_congruence

with open('input.txt') as f:
    raw = f.read().split('\n')


def part_a():
    depart = int(raw[0])
    busses = [int(b) for b in raw[1].split(',') if b != 'x']
    min_to_next = {x: x - (depart % x) for x in busses}
    bus = min(min_to_next, key=min_to_next.get)
    print(bus * min_to_next[bus])


def part_b_bf(timestamp: int):
    schedule = dict(zip([b for b in raw[1].split(',')], range(len(raw[1]))))
    schedule = {int(k): v for k, v in schedule.items() if k != 'x'}
    freq = int(raw[1][0])
    while timestamp % freq:
        timestamp += 1

    while True:
        start = timestamp + freq
        found = True
        for k in schedule.keys():
            if (k - start % k) % k != schedule[k]:
                found = False
                break
        if found:
            print(start)
            return
        timestamp += 1

        # min_to_next = {k: (k - (start % k)) % k for k in schedule.keys()}
        # if min_to_next == schedule:
        #     print(start)
        #     return
        # timestamp += 1


def part_b():
    schedule = dict(zip([b if b == 'x' else int(b) for b in raw[1].split(',')], range(len(raw[1]))))
    schedule = {k: (k-v) % k for k, v in schedule.items() if k != 'x'}

    min_freq = max(schedule.keys())
    other_busses = [b for b in schedule.keys() if b != min_freq]
    while True:
        increment = min_freq
        timestamp = schedule[min_freq]
        for b in other_busses:
            while timestamp % b != schedule[b]:
                timestamp += increment
            increment *= b
        print(timestamp)
        break


def part_b_crt():
    schedule = dict(zip([b if b == 'x' else int(b) for b in raw[1].split(',')], range(len(raw[1]))))
    schedule = {k: (k-v) % k for k, v in schedule.items() if k != 'x'}
    busses = list(schedule.keys())
    remainders = [schedule[b] for b in busses]
    print(crt(busses, remainders)[0])


def part_b_congruence():
    schedule = dict(zip([b if b == 'x' else int(b) for b in raw[1].split(',')], range(len(raw[1]))))
    schedule = {(k-v) % k: k for k, v in schedule.items() if k != 'x'}
    print(solve_congruence(*tuple(schedule.items()))[0])


part_a()
part_b()
part_b_crt()
part_b_congruence()
