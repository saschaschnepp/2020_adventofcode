import functools, time
from typing import Tuple

sample = '389125467'
input_ = '219748365'

cups = input_
n_cups = len(cups)


def timer(func):
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        tic = time.perf_counter()
        value = func(*args, **kwargs)
        toc = time.perf_counter()
        elapsed_time = toc - tic
        print(f"Elapsed time: {elapsed_time:0.4f} seconds")
        return value
    return wrapper_timer


def pickup(cups_: str) -> Tuple[str, str]:
    pickup_ = cups_[1:4]
    rem_ = cups_.replace(pickup_, '')
    return rem_, pickup_


def find_destination(rem_: str) -> int:
    dest_ = int(rem_[0])
    seq_ = ''.join(map(str, (list(range(dest_ - 1, 0, -1)) + list(range(9, dest_ - 1, -1)))))
    for s_ in seq_:
        try:
            return rem_.index(s_)
        except ValueError:
            pass
    raise Exception("This shouldn't happen")


def insert_pickup(rem_: str, pickup_: str, id_: int) -> str:
    tmp = rem_[:id_+1] + pickup_ + rem_[id_+1:]
    return tmp[1:] + tmp[0]


@timer
def part_a(cups_):
    for move in range(100):
        rem, pick = pickup(cups_)
        dest = find_destination(rem)
        cups_ = insert_pickup(rem, pick, dest)
    return cups_


cups = part_a(cups)
id_ = (cups + cups).index('1')
print('Part A: ' + (cups + cups)[id_+1:id_+9])
