from __future__ import annotations
from typing import Tuple, List, Dict

sample = '389125467'
input_ = '219748365'

# cups = [int(c) for c in sample]
cups = [int(c) for c in input_]
n_cups = len(cups)


class Node:

    def __init__(self, n_):
        self.n = n_
        self.prev = self.next = self

    def __repr__(self):
        return f'{self.n}, p:{self.prev.n}, n:{self.next.n}'

    def insert_after(self, other: Node):
        other.prev = self
        other.next = self.next
        self.next.prev = other
        self.next = other


def setup(cups_) -> Tuple[List, Dict[int, Node]]:
    cups_ += list(range(max(cups_)+1, 1_000_001))
    cup_dct_ = {c: Node(c) for c in cups_}  # Dictionary order is guaranteed to be insertion order from Python 3.7
    prev = None
    for i_, c_ in enumerate(cups_):
        if prev is None:
            prev = c_
            continue
        cup_dct_[prev].insert_after(cup_dct_[c_])
        prev = c_
    return cups_, cup_dct_


def move(curr_: Node):
    cup1 = curr_.next
    cup2 = cup1.next
    cup3 = cup2.next
    picked_n = tuple([cup1.n, cup2.n, cup3.n])
    curr_.next = cup3.next
    curr_.next.prev = curr_
    dest_n = curr_.n - 1 or len(cups)
    while dest_n in picked_n:
        dest_n = dest_n - 1 or len(cups)
    dest_ = cup_dct[dest_n]
    cup3.next = dest_.next
    cup3.next.prev = cup3
    dest_.next = cup1
    cup1.prev = dest_


def print_dll(start_: Node):
    lst_ = []
    curr_ = start_
    while True:
        lst_.append(curr_.n)
        curr_ = curr_.next
        if curr_ == start_:
            break
    print(''.join(map(str, lst_)))


cups, cup_dct = setup(cups)
# print_dll(cup_dct[cups[0]])
curr = cup_dct[cups[0]]
for m_ in range(10_000_000):
    if not m_ % 1_000_000:
        print(f'Step: {m_}')
    move(curr)
    curr = curr.next

# print_dll(cup_dct[0])
print(f'Part B: {cup_dct[1].next.n}, {cup_dct[1].next.next.n}')
print(f'Part B: {cup_dct[1].next.n * cup_dct[1].next.next.n}')
