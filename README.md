# Advent of Code 2020 in python

I'm posting my solutions for the [Advent of Code 2020](https://adventofcode.com/2020) for comparison or for finding inspiration should you get stuck on a riddle. 
The fun is in solving them yourself ;)

Solutions tested on `python3.8`.

I attempted to use functional concepts like `map` and `reduce` as much as possible.

## Solutions

| Day | Notes |
| --- | ----- |
| [Day 1](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day1/) | Using `itertools.combinations` is nice option. |
| [Day 2](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day2/) | Regular expression for decomposition. |
| [Day 3](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day3/) | Make the stride an argument. (I switched to storing input in a file from here) |
| [Day 4](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day4/) | Regexs for the decomposition in part B |
| [Day 5](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day5/) | `replace` to make a binary number, then `int(n, 2)` to convert. |
| [Day 6](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day6/) | Using `set` and `map`, `reduce`, part A becomes a one-liner. B uses set intersections. |
| [Day 7](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day7/) | Very nice one. Regex for the decomposition. `networkx` as graph lib. Not reinventing the wheel... A and B are opposites in the sense that one looks for all incoming and the other for all outgoing paths. |
| [Day 8](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day8/) | No particular trick or specific data structure usage I could find... |
| [Day 9](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day9/) | Using `itertools.combinations` again. |
| [Day 10](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day10/) | Dynamic programming, memoizing, caching, or however you prefer calling it to the rescue (`lru_cache` in python) |
| [Day 11](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day11/) | Game of Life #1 ([RIP John Conway](https://arstechnica.com/science/2020/04/john-conway-inventor-of-the-game-of-life-has-died-of-covid-19/)). Remeber to `deepcopy`. |
| [Day 12](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day12/) | Time to write a `class`. Use a [rotation matrix](https://en.wikipedia.org/wiki/Rotation_matrix). |
| [Day 13](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day13/) | Part A is simple and part B has been a time-killer for me. If [Chinese-Remainder-Theorem](https://en.wikipedia.org/wiki/Chinese_remainder_theorem) or congruence problems ring a bell for you - lucky you. Brute-force iteration doesn't do it, takes way too long. However, the increment can successively be multiplied by the bus frequencies as the condition for the remainders is true every `freq_1 * freq_2 * ...` timesteps. |
| [Day 14](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day14/) | Conversion between binary number as a string and ints. |
| [Day 15](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day15/) | The solution has a condition on the last number in the starting sequence. It solves the problem though ;) |
| [Day 16](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day16/) | It was probably clear that the invalid tickets will be needed in some way in part B but I didn't return them explicitly in part A, so part B basically reimplements part A to exclude the invalid tickets first. Part B finds feasible mappings based on field ranges and ticket field values and then recursively makes assignments using the fact that the feasibility set for one field includes just one option. |
| [Day 17](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day17/) | Game of Life #2, this time multi-dimensional. The solution works for any dimension (but will be very slow at some point). A 5D run is included. The solution uses `numpy` and slicing for setting the initial data. |
| [Day 18](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day18/) | For some reason that I don't understand anymore myself, I didn't want to use stacks and the Shunting-Yard Algorithm but wanted to build a tree representation just left to right and then implement the grammar into the evaluation rules of the tree. Don't know what I'm talking about? Well, me neither as I had to recognize. Still not wanting to implement the obvious, I noticed that implementing a number class with custom `__add__` and `__mul__` methods does the trick. |
| [Day 19](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day19/) | A number of concepts are used in this one. `itertools.product` for building tensor products of lists (used for concatenating lists such as `a b` where `a` and `b` are lists), recursion, and regex matching - and all of this in part A already. <br /> Part B introduces recursion into the rules. Simply replacing the rules obviously leads to a recursion error. Looking at the specific rule pattern introduced by the rule modifications we see that `rule 8` corresponds to an arbitrary number of repetitions of `rule 42`. `Rule 11` corresponds to the same but arbitrary number of repetitions of `rule 42` and `rule 31` concatenated (e.g. `42, 42, 42, 31, 31, 31`). For `rule 0` this means that we have `n` repetitions of `42` followed by `m` repetitions of `31` where `n > m`. <br />General remark: A more general and elegant solution would use only regular expressions and particularly recursive regular expressions offered by the [`regex`](https://bitbucket.org/mrabarnett/mrab-regex/src/hg/) package. |
| [Day 20](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day20/) | There isn't anything particularly difficult about this one, but it is a bit of code to write. I am using `numpy` for representing the tiles and `numpy.rot90` and `numpy.flipud` for rotating and flipping tiles. For part B, I start with the north-western corner tile (identified by having only an eastern and southern neighbor) and assemble first row by finding the respective eastern neighbor. For all other rows, the row is assembled eastwards starting with the southern neighbor of the respective previous row's first (western) tile. The Nessie monsters are identified through multi-line regex matching. I'm shifting the Nessie pattern left to right. A more skilled regex user might manage a nicer way using regex look-ahead. |
| [Day 21](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day21/) | Regex for input decomposition. Set intersection and recursive assignment for the solution. |
| [Day 22](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day22/) | Dynamic programming for speedup and `deepcopy`, no specific trick. |
| [Day 23](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day23/) | Part A solved by straight-forward string manipulation. Part B is essentially the same but with a much larger input and many more rounds making the string-based solution from part A too slow. Specifically, it is finding the destination which takes too long. Taking out cups and inserting them is `O(0)` but the destination search is `O(n)`. For 1M cups and 10M rounds this is not possible. I tried to find some closed-form solution to the arrangement algorithm but couldn't find one. Hence, a solution needs to reduce the destination search to constant runtime. A solution is to implement the cups as a circular doubly-linked list and also keeping the nodes in a map (dict) indexed by the cup value. |
| [Day 24](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day24/) | Using `collections.Counter` combined with regex for decomposing the input. Tile positions can be computed from the number of occurrences of each direction. The lobby itself is represented by a `collections.defaultdict` with default `False`.<br />Part B is the third occasion of Game of Life. |
| [Day 25](https://bitbucket.org/saschaschnepp/2020_adventofcode/src/master/Day25/) | A well-deserved easy one. |

## Feedback
Happy for feedback. Visit [my page](http://www.saschaschnepp.net/about/) for contact info (Sorry page is very much neglected).

## License
[MIT](https://choosealicense.com/licenses/mit/) (TL;DR: Do want you wish with the code while you don't sue me.) 