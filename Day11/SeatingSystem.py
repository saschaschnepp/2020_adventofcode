import numpy as np
from itertools import product
from copy import deepcopy

with open('input.txt') as f:
    data = np.array(list(map(list, f.read().split('\n'))))


def get_adjacent(idx: tuple, data_: np.array) -> np.array:

    r = (max(0, idx[0] - 1), min(data.shape[0], idx[0] + 2))
    c = (max(0, idx[1] - 1), min(data.shape[1], idx[1] + 2))
    indices = [i for i in product(range(r[0], r[1]), range(c[0], c[1])) if i != idx]
    return np.array([data_[x] for x in indices])


def in_field(idx):
    if 0 <= idx[0] < data.shape[0] and 0 <= idx[1] < data.shape[1]:
        return True
    else:
        return False


def get_line_sight(idx_: tuple, data_: np.array):

    occupied = 0
    for stride in map(np.asarray, product([-1, 0, 1], [-1, 0, 1])):
        if (stride == (0, 0)).all():
            continue

        id_ = np.asarray(idx_) + stride
        while in_field(id_):
            if data_[tuple(id_)] == 'L':
                break
            if data_[tuple(id_)] == '#':
                occupied += 1
                break
            id_ = id_ + stride

    return occupied


def count_elem(data_: np.array, tgt):
    return (data_ == tgt).sum()


def count_free(data_: np.array):
    return count_elem(data_, 'L')


def count_occupied(data_: np.array):
    return count_elem(data_, '#')


def next_state_a(idx_: tuple, data_: np.array):
    if data_[idx_] == '.':
        return '.'
    cnt = count_occupied(get_adjacent(idx_, data_))
    if cnt == 0:
        return '#'
    elif 3 < cnt:
        return 'L'
    else:
        return data_[idx_]


def next_state_b(idx_: tuple, data_: np.array):
    if data_[idx_] == '.':
        return '.'
    cnt = get_line_sight(idx_, data_)
    if cnt == 0:
        return '#'
    elif 4 < cnt:
        return 'L'
    else:
        return data_[idx_]


def part_a(data_):
    copy = deepcopy(data)
    while True:
        for idx in product(range(data_.shape[0]), range(data_.shape[1])):
            copy[idx] = next_state_a(idx, data_)
        if (data_ == copy).all():
            break
        data_ = deepcopy(copy)
    print(count_occupied(data_))


def part_b(data_):
    copy = deepcopy(data)
    while True:
        for idx in product(range(data_.shape[0]), range(data_.shape[1])):
            copy[idx] = next_state_b(idx, data_)
        if (data_ == copy).all():
            break
        # print(copy)
        data_ = deepcopy(copy)
    print(count_occupied(data_))


# part_a(data)
part_b(data)
