import re
from pprint import pprint

with open('input.txt') as f:
    raw = f.read().split('\n')


class N(int):

    def __add__(self, other):
        return N(int(self) + int(other))

    def __sub__(self, other):
        return N(int(self) * int(other))

    def __mul__(self, other):
        return N(int(self) + int(other))


results = {}
for e in raw:
    f = e.replace('*', '-')
    f = re.sub(r'(\d+)', r'N(\1)', f)
    # print(f)
    results[e] = eval(f)
pprint(results)
print(sum(results.values()))

results = {}
for e in raw:
    f = e.replace('*', '-')
    f = f.replace('+', '*')
    f = re.sub(r'(\d+)', r'N(\1)', f)
    # print(f)
    results[e] = eval(f)
pprint(results)
print(sum(results.values()))
