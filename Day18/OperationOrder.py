from operator import mul, add
from typing import Optional, Callable
from pprint import pprint

with open('input.txt') as f:
    raw = f.read().split('\n')

ops = dict(zip(('*', '+'), (mul, add)))


def strip_superfluous_parentheses(exp_: str) -> str:

    while exp_[0] == '(' and get_parentheses_subexp(exp_) == exp_:
        exp_ = exp_[1:-1]
    return exp_


def get_parentheses_subexp(exp_: str) -> str:

    def recurse(exp__: str, accumulator: str, opening: int) -> str:
        if not exp__:
            raise StopIteration("Expression terminated with unmatched parentheses")

        if opening == 1 and exp__[0] == ')':
            return accumulator + exp__[0]
        elif exp__[0] == ')':
            return recurse(exp__[1:], accumulator + exp__[0], opening - 1)
        elif exp__[0] == '(':
            return recurse(exp__[1:], accumulator + exp__[0], opening + 1)
        else:
            return recurse(exp__[1:], accumulator + exp__[0], opening)

    return recurse(exp_[1:], '(', 1)


class BiNode:

    def __init__(self, exp_: str):
        self.exp: str = strip_superfluous_parentheses(exp_.strip())
        self.left: Optional[BiNode] = None
        self.right: Optional[BiNode] = None
        self.left_exp = ''
        self.right_exp = ''
        self.op: Optional[Callable] = None
        self.value = None
        self.opening = len(tuple(p for p in self.exp if p == '('))
        self.closing = len(tuple(p for p in self.exp if p == ')'))
        assert self.opening == self.closing


def parse(node_: BiNode):

    try:
        node_.value = int(node_.exp)
        return
    except ValueError:
        pass

    if node_.exp[0] == '(':
        right_exp = get_parentheses_subexp(node_.exp)
        node_.right_exp = right_exp
    else:
        right_exp = node_.exp[0]
        node_.right_exp = right_exp
    remainder = node_.exp[len(right_exp):].strip()

    assert remainder[0] in ops
    node_.op = ops[remainder[0]]

    remainder = remainder[1:].strip()
    node_.left_exp = remainder

    node_.right = BiNode(node_.right_exp)
    node_.left = BiNode(node_.left_exp)

    parse(node_.left)
    parse(node_.right)

    return


def eval_parsed(node_: BiNode) -> int:

    if node_.left is None:
        assert node_.right is None
        assert len(node_.exp) == 1
        node_.value = int(node_.exp)
        return node_.value

    return node_.op(
        eval_parsed(node_.left),
        eval_parsed(node_.right)
    )


# def add_parentheses_precedence(exp_: str) -> str:
#
#     result = ''
#     id_ = 0
#     while id_ < len(exp_):
#         if result[id_] != '+':
#             result += exp[id_]
#         else:
#             result = result[:-2] + '(' + result[-2:] + ' '
#             id_ += 1
#             if exp_[id_ + 1] == '(':
#                 sub_exp = get_parentheses_subexp(exp_[id_ + 1:])
#
#
#     return result


results = {}
for exp in raw:
    bi_tree = BiNode(exp.replace('(', '[').replace(')', '(').replace('[', ')')[::-1])
    parse(bi_tree)
    results[exp] = eval_parsed(bi_tree)
print(sum(results.values()))

results = {}
for exp in raw:
    exp = add_parentheses_precedence(exp)
    bi_tree = BiNode(exp.replace('(', '[').replace(')', '(').replace('[', ')')[::-1])
    parse(bi_tree)
    results[exp] = eval_parsed(bi_tree)
print(sum(results.values()))
