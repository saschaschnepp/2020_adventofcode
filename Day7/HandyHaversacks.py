import re
import networkx as nx
# import matplotlib.pyplot as plt

with open('input.txt') as f:
    raw = f.read().split('\n')


def graph_weight(n1, w1, G: nx.DiGraph):

    if G.out_degree(n1) == 0:
        return w1

    sum_weights = 0
    for n2 in G.neighbors(n1):
        sum_weights += graph_weight(n2, G.get_edge_data(n1, n2)['weight'], G)
    return w1 * sum_weights + w1


def print_path(p, G):
    i = 0
    pth = ""
    while i < len(p) - 1:
        e = G.get_edge_data(p[i], p[i + 1])
        pth += f'{p[i]} -{e["weight"]}-> '
        i += 1
    pth += p[i]
    print(pth)


def part_a():
    G = nx.DiGraph()
    in_ = re.compile(r"\d+ \w+ \w+ bag\.*")
    for line in raw:
        left = ' '.join(line.split(" ")[:2])
        for l_ in in_.findall(line):
            right = ' '.join(l_.split(" ")[1:3])
            G.add_edge(left, right)
    starting_nodes = [path[0] for g in G.nodes() for path in nx.all_simple_paths(G, g, 'shiny gold')]
    print(len(set(starting_nodes)))


def part_b():
    G = nx.DiGraph()
    in_ = re.compile(r"\d+ \w+ \w+ bag\.*")
    for line in raw:
        left = ' '.join(line.split(" ")[:2])
        for l_ in in_.findall(line):
            splits = l_.split(" ")
            right = ' '.join(splits[1:3])
            w = int(splits[0])
            G.add_edge(left, right, weight=w)

    print(graph_weight('shiny gold', 1, G) - 1)


part_a()
part_b()

# pos = nx.spring_layout(G)
# nx.draw_networkx_labels(G, pos)
# nx.draw_networkx_edges(G, pos, arrows=True)
# plt.show()
