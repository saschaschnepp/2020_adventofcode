import numpy as np
from itertools import product
from copy import deepcopy

with open('input.txt') as f:
    raw = np.asarray(list(
        map(
            lambda x: [int(m) for m in x],
            map(
                lambda x: x.replace('.', '0').replace('#', '1'),
                f.read().split('\n')
            )
        )
    ), dtype=np.int8)
init_size = raw.shape
init = np.expand_dims(raw, len(raw.shape))


class Grid:

    def __init__(self, init_: np.ndarray, cycles_: int):

        self.init = init_
        self.cycles = cycles_
        self.init_size = init_.shape
        self.max_size = tuple(n + 2 * cycles_ for n in init_.shape)
        self.center = tuple(self.max_size[i]//2 for i, _ in enumerate(init_.shape))
        self.grid = np.zeros(self.max_size, dtype=np.int8)
        init_range = [slice(
            (self.center[i] - self.init_size[i] // 2),
            (self.center[i] + np.rint(self.init_size[i] / 2).astype(int)))
             for i in [0, 1]
        ]
        init_range += [self.center[i] for i in range(2, len(init_.shape))]
        init_range = tuple(init_range)
        self.grid[init_range] = init_.flatten().reshape(init_size)

        self.next_grid = None
        # print(self.grid[init_range])

    def get_adjacent(self, idx: tuple) -> np.array:

        ranges = [range(max(0, i - 1), min(self.max_size[d], i + 2)) for d, i in enumerate(idx)]
        indices = [i for i in product(*ranges) if i != idx]
        return np.array([self.grid[x] for x in indices])

    def get_state(self, idx: tuple):
        if self.grid[idx]:
            return 'active'
        else:
            return 'inactive'

    def next_state(self, idx: tuple):

        num = self.get_adjacent(idx).sum()
        if self.get_state(idx) == 'active' and num in [2, 3]:
            return 1
        elif self.get_state(idx) == 'inactive' and num == 3:
            return 1
        else:
            return 0

    def update(self):
        self.next_grid = deepcopy(self.grid)
        for idx, _ in np.ndenumerate(self.grid):
            self.next_grid[idx] = self.next_state(idx)
        self.grid = self.next_grid

    def process(self):

        for c in range(self.cycles):
            self.update()


def part_a(init_):
    g = Grid(init_, 6)
    g.process()
    print(g.grid.sum())


def part_b(init_):
    init_ = np.expand_dims(init_, len(init_.shape))
    g = Grid(init_, 6)
    g.process()
    print(g.grid.sum())


def dim_five(init_):
    init_ = np.expand_dims(init_, len(init_.shape))
    init_ = np.expand_dims(init_, len(init_.shape))
    g = Grid(init_, 3)
    g.process()
    print(g.grid.sum())


if __name__ == '__main__':
    part_a(init)
    part_b(init)
    dim_five(init)
