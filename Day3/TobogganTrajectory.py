from functools import reduce
from operator import mul

data = []
with open('Input.txt') as f:
    for row in f:
        data.append(row.strip())

width = len(data[0])


def get_trees(start_, stride_):
    trees_ = 0
    r = start_[0]
    c = start_[1]
    for r_ in range(r, len(data), stride_[0]):
        trees_ += int(data[r_][c] == '#')
        c = (c + stride_[1]) % width
    return trees_


strt = [0, 0]
strides = [
    [1, 1],
    [1, 3],
    [1, 5],
    [1, 7],
    [2, 1]
]

trees = []
for stride in strides:
    trees.append(get_trees(strt, stride))

print(reduce(mul, trees))
