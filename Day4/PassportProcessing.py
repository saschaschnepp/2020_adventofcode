import re

data = []
with open('input.txt') as f:
    raw = f.read().split('\n\n')
    # for row in f:
    #     data.append(row.strip())

fields = """byr
iyr
eyr
hgt
hcl
ecl
pid""".split('\n')
fields.sort()

optional = ["cid"]


def a():
    valid = 0
    for pp in raw:
        pp = pp.replace("\n", " ").split(':')[:-1]
        key = [k.split(" ")[1] for k in pp[1:]]
        key = key + [pp[0]]
        key = list(filter(lambda x: x not in optional, key))
        key.sort()
        if fields == key:
            valid += 1
    return valid


match_in = re.compile("[5-7][0-9]in")
match_cm = re.compile("1[5-9][0-9]cm")
match_hcl = re.compile(r"^\#[0-9a-f]{6}$")
match_pid = re.compile(r"^\d{9}$")
validation = {
    "byr": lambda x: 1920 <= int(x) <= 2002,
    "iyr": lambda x: 2010 <= int(x) <= 2020,
    "eyr": lambda x: 2020 <= int(x) <= 2030,
    "hgt": lambda x: (bool(match_in.match(x)) and 59 <= int(x[:2]) <= 76) or (bool(match_cm.match(x)) and 150 <= int(x[:3]) <= 193),
    "hcl": lambda x: bool(match_hcl.match(x)),
    "ecl": lambda x: x in "amb blu brn gry grn hzl oth".split(" "),
    "pid": lambda x: bool(match_pid.match(x))
}


def b():
    valid = 0
    for pp in raw:
        pp = pp.replace("\n", " ")
        dct = {p.split(":")[0]:p.split(":")[1] for p in pp.split(" ")}

        # check keys
        key = list(dct.keys())
        key = list(filter(lambda x: x not in optional, key))
        key.sort()
        if fields != key:
            continue
        dct = {k:dct[k] for k in key}

        # check values
        ok = True
        for k, v in dct.items():
            if not validation[k](v):
                ok = False
                break
        if ok:
            valid += 1

    return valid


valid = a()
print(valid)

valid = b()
print(valid)





