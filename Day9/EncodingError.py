from itertools import combinations

with open('input.txt') as f:
    raw = list(map(int, f.read().split('\n')))


def part_a():
    tpls = 2
    num = 25
    idx = num - 1
    buffer = raw[:num]
    valid = [False] * len(raw)
    valid[:num] = [True] * num
    for i in range(num, len(raw)):
        tgt = raw[i]
        for tpl in combinations(buffer, tpls):
            if sum(list(tpl)) == tgt:
                valid[i] = True
                break
        idx = (idx + 1) % num
        buffer[idx] = tgt

    return [raw[i] for i, v in enumerate(valid) if v is False]


def part_b():
    invalid = part_a()[0]
    idx_invalid = raw.index(invalid)

    max_seq_length = idx_invalid - 1
    for seq_len in range(max_seq_length, 1, -1):
        for start in range(idx_invalid-seq_len):
            seq = raw[start:start + seq_len]
            if sum(seq) == invalid:
                return min(seq), max(seq), min(seq) + max(seq)


print(part_a())
print(part_b())
