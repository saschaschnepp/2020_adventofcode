from functools import reduce
from operator import add

with open('input.txt') as f:
    raw = f.read().split('\n')


def to_num(txt):
    return int(txt[:7].replace("F", "0").replace("B", "1"), 2), int(txt[-3:].replace("L", "0").replace("R", "1"), 2)


def seat_id(txt):
    digits = to_num(txt)
    return digits[0]*8 + digits[1]


taken = sorted([id_ for id_ in map(seat_id, raw)])
print(taken[-1])

for i, s in enumerate(taken[:-2]):
    if reduce(add, taken[i:i+3]) != 3*(s+1):
        print(taken[i:i+3])
