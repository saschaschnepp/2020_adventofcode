import functools
import time

from itertools import combinations

def timer(func):
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        tic = time.perf_counter()
        value = func(*args, **kwargs)
        toc = time.perf_counter()
        elapsed_time = toc - tic
        print(f"Elapsed time: {elapsed_time:0.4f} seconds")
        return value
    return wrapper_timer


data = [1788,
        1627,
        1883,
        1828,
        1924,
        1993,
        972,
        1840,
        1866,
        1762,
        1781,
        1782,
        1520,
        1971,
        1660,
        1857,
        1867,
        1564,
        1983,
        1391,
        2002,
        494,
        1500,
        1967,
        1702,
        1958,
        1886,
        1910,
        1838,
        1985,
        1836,
        2009,
        2005,
        1602,
        1939,
        1945,
        1609,
        1582,
        1647,
        1737,
        1982,
        1931,
        790,
        745,
        1598,
        1586,
        1547,
        1951,
        1264,
        1382,
        1776,
        1499,
        1977,
        1766,
        1360,
        1807,
        1991,
        1981,
        1693,
        634,
        1847,
        1774,
        1990,
        1409,
        1410,
        1974,
        1862,
        1744,
        1827,
        1978,
        1980,
        2003,
        1491,
        1595,
        1640,
        1576,
        1887,
        1746,
        1617,
        1923,
        1706,
        1964,
        60,
        1620,
        1959,
        257,
        1395,
        1854,
        1843,
        1682,
        1667,
        1639,
        279,
        1911,
        1986,
        1575,
        1232,
        1919,
        1852,
        1509,
        1976,
        1465,
        2008,
        1953,
        1518,
        1795,
        1912,
        1269,
        1835,
        1984,
        1538,
        2001,
        1954,
        1365,
        1569,
        1418,
        1844,
        1580,
        1875,
        1551,
        1861,
        1946,
        1810,
        1655,
        1987,
        1549,
        1301,
        1859,
        1929,
        1254,
        1604,
        1933,
        1998,
        1661,
        1899,
        1411,
        1975,
        1707,
        1966,
        1601,
        1936,
        1440,
        1942,
        1937,
        1851,
        1731,
        1257,
        1533,
        1405,
        1890,
        1600,
        1970,
        1626,
        1824,
        1442,
        2006,
        1796,
        1658,
        1930,
        646,
        1904,
        1489,
        2004,
        1922,
        1424,
        1802,
        1623,
        1870,
        1242,
        1591,
        1338,
        754,
        1826,
        1305,
        1825,
        1793,
        1872,
        1741,
        1979,
        107,
        1833,
        1856,
        1952,
        1791,
        1728,
        2010,
        1965,
        1646,
        1522,
        1671,
        1624,
        1876,
        1537,
        1759,
        1962,
        1773,
        1907,
        1573,
        1908,
        1903,
        ]


@timer
def zipped_a():
    found = False
    for i, k in enumerate(data):
        for l in zip([k]*(len(data)-i+1), data[i+1:]):
            if sum(list(l)) == 2020:
                print(l[0] * l[1])
                found = True
                break
        if found:
            break


@timer
def simple_a():
    found = False
    for i, j in enumerate(data):
        for _, l in enumerate(data[i+1:]):
            if j+l == 2020:
                print(j*l)
                found = True
                break
        if found:
            break


@timer
def simple_b():
    found = False
    for i, j in enumerate(data):
        data_i_ = data[i + 1:]
        for k, l in enumerate(data_i_):
            for m, n in enumerate(data_i_[k+1:]):
                if j+l+n == 2020:
                    print(j*l*n)
                    found = True
                    break
            if found:
                break
        if found:
            break

def mult_rec(lst):
    if not lst:
        return 0

    if len(lst) == 1:
        return lst[0]

    return lst[0] * mult_rec(lst[1:])


@timer
def combs(n: int):
    for tpl in combinations(data, n):
        if sum(list(tpl)) == 2020:
            return mult_rec(list(tpl))


zipped_a()
simple_a()
simple_b()
print(combs(2))
print(combs(3))
