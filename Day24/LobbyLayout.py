import re
from collections import defaultdict, Counter
from typing import Tuple, Generator, Dict

with open('input.txt') as f:
    lines = f.read().split('\n')

lobby = defaultdict(lambda: False)
line_dirs = []
for line in lines:
    count = Counter()
    count.update(re.findall('(se|sw|ne|nw|e|w)', line))
    line_dirs.append(count)
# pprint(line_dirs)

# Part A
for line in line_dirs:
    ns = line['ne'] + line['nw'] - line['se'] - line['sw']
    ew = line['ne'] + line['se'] + 2*line['e'] - line['nw'] - line['sw'] - 2*line['w']
    lobby[(ns, ew)] = not lobby[(ns, ew)]
black = sum(lobby.values())
print(f'Part A: {black}')

###########################
# Part B


def get_neighbors(pos_: Tuple[int, int]) -> Generator[Tuple[int, int], None, None]:
    yield pos_[0] + 1, pos_[1] + 1
    yield pos_[0] + 1, pos_[1] - 1
    yield pos_[0] - 1, pos_[1] - 1
    yield pos_[0] - 1, pos_[1] + 1
    yield pos_[0], pos_[1] + 2
    yield pos_[0], pos_[1] - 2


def next_day(lobby_: Dict[Tuple[int, int], bool]) -> Dict[Tuple[int, int], bool]:
    _lobby = defaultdict(lambda: False)
    for tile_, col_ in lobby_.items():
        count_ = sum([lobby_[n_] for n_ in get_neighbors(tile_) if n_ in lobby_])
        if col_:  # black
            if count_ in (1, 2):
                _lobby[tile_] = True
        else:  # white
            if count_ == 2:
                _lobby[tile_] = True
        # Need to check neighbors of the tile too as only black tiles are in lobby_
        for n_tile_ in get_neighbors(tile_):
            if n_tile_ in lobby_:
                continue
            count_ = sum([lobby_[n_] for n_ in get_neighbors(n_tile_) if n_ in lobby_])
            if count_ == 2:  # As tiles already in lobby_ are excluded these are all white tiles
                _lobby[n_tile_] = True

    return _lobby


for i in range(100):
    lobby = next_day(lobby)
    print(f'Day {i+1}: {sum(lobby.values())}')
