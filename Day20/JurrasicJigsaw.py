# This solution can (or should be) cleaned up by I'll pass.

import re
import numpy as np
from functools import reduce
from copy import deepcopy
from operator import mul
from math import isqrt
from typing import Dict, List, Generator, Optional

with open('input.txt') as f:
    raw = f.read().split('\n\n')
tiles_raw = {}
for r in raw:
    t = r.split('\n')
    tiles_raw[int(re.search(r'(\d+)', t[0])[1])] = t[1:]

##################################################
# Tile


class Tile:

    def __init__(self, id_: int,  t_: List[List[str]]):
        self.id = id_
        self.matrix = np.asarray(t_)
        self.orig = deepcopy(self.matrix)
        self.edges = self.get_edges()
        self.neighbors = {}

    def rows_as_string(self):
        return [''.join(row_) for row_ in self.matrix]

    def __repr__(self):
        return '\n'.join(self.rows_as_string())

    def _rotate(self) -> np.ndarray:
        self.matrix = np.rot90(self.matrix)
        return self.matrix

    def flip(self) -> np.ndarray:
        self.matrix = np.flipud(self.matrix)
        return self.matrix

    def orientations(self) -> Generator[np.ndarray, None, None]:
        yield self.matrix
        for i in range(3):
            yield self._rotate()
        yield self.flip()
        for i in range(3):
            yield self._rotate()

    def trim_edges(self) -> np.ndarray:
        self.matrix = self.matrix[1:-1, 1:-1]
        return self.matrix

    def get_edges(self) -> Dict[str, str]:
        self.edges = {
            'N': ''.join(self.matrix[0]),
            'E': ''.join(self.matrix[:, -1]),
            'S': ''.join(self.matrix[-1]),
            'W': ''.join(self.matrix[:, 0])
        }
        return self.edges

    def neighbor_at(self, dir_: str, tiles_: dict):
        edge_self = self.get_edges()[dir_]
        for n_ in self.neighbors.values():
            tile_l = tiles_[n_]
            edges_l = list(tile_l.get_edges().values())
            edges_l += [e[::-1] for e in edges_l]
            for edge_l in edges_l:
                if edge_self == edge_l:
                    return tiles_[n_]

    def restore(self):
        self.matrix = deepcopy(self.orig)


##################################################
# Part A

tiles = {}
for k_, t in tiles_raw.items():
    tile = []
    for row in t:
        chars = []
        for ch in row:
            chars.append(ch)
        tile.append(chars)
    tiles[k_] = Tile(k_, tile)


def find_neigbors(tiles_: Dict[int, Tile]):
    keys = list(tiles_.keys())
    for i, k in enumerate(keys):
        tile_k = tiles_[k]
        for j, l in enumerate(keys):
            if k == l:
                continue
            tile_l = tiles_[keys[j]]
            edges_l = list(tile_l.get_edges().values())
            edges_l += [e[::-1] for e in edges_l]
            for side_k, edge_k in tile_k.get_edges().items():
                for edge_l in edges_l:
                    if edge_k == edge_l:
                        tile_k.neighbors[side_k] = tile_l.id
                        break


find_neigbors(tiles)
corners = [i for i, t in tiles.items() if len(t.neighbors) == 2]
print('Part A: ', reduce(mul, corners), corners)

##############################################
# Part B

# Assemble Grid #
side_length = isqrt(len(tiles))
remaining_tiles = list(tiles.keys())
corner_tiles = {}
for c in corners:
    if {'E', 'S'} == set(tiles[c].neighbors.keys()):
        corner_tiles['NW'] = tiles[c]
        break
assert 'NW' in corner_tiles


def get_eastern_tile(tile_w: Tile) -> Tile:
    """Get the eastern tile and orient it to match."""

    tile_e_ = tile_w.neighbor_at('E', tiles)
    for _ in tile_e_.orientations():
        edges_e = tile_e_.get_edges()
        if tile_w.edges['E'] == edges_e['W']:
            return tile_e_


def get_southern_tile(tile_n: Tile) -> Tile:
    """Get the southern tile and orient it to match."""

    tile_s = tile_n.neighbor_at('S', tiles)
    for _ in tile_s.orientations():
        edges_s = tile_s.get_edges()
        if tile_n.edges['S'] == edges_s['N']:
            return tile_s


def get_first_tile(tile_: Optional[Tile]) -> Tile:
    if not tile_:
        return corner_tiles['NW']
    else:
        return get_southern_tile(tile_)


rows = []
first = None
for row in range(side_length):
    subrow = []
    first = get_first_tile(first)
    t_ = first
    subrow.append(first)
    for _ in range(1, side_length):
        t_ = get_eastern_tile(t_)
        subrow.append(t_)
    rows.append(subrow)

# mat = []
# for row in rows:
#     mat.append([t_.id for t_ in row])
# grid = np.asarray(mat)
# print(grid)

# Trim tiles #
for t_ in tiles.values():
    t_.trim_edges()

# Make picture from tiles #
pic_rows = []
for row in rows:
    tile_row = np.asarray([c.rows_as_string() for c in row]).T
    pic_row = '\n'.join([''.join(line_) for line_ in tile_row])
    pic_rows.append(pic_row)
pic = '\n'.join(pic_rows)


# Turn and flip pic #
def _array_from_pic(pic_: str) -> np.ndarray:
    return np.asarray([list(x) for x in pic_.splitlines()])


def _pic_from_array(arr_: np.ndarray) -> str:
    return '\n'.join([''.join(row) for row in arr_])


def pic_orientations(pic_: str) -> Generator[str, None, None]:
    arr_ = _array_from_pic(pic_)
    yield pic_
    for i in range(3):
        arr_ = np.rot90(arr_)
        yield _pic_from_array(arr_)
    arr_ = np.flipud(arr_)
    yield _pic_from_array(arr_)
    for i in range(3):
        arr_ = np.rot90(arr_)
        yield _pic_from_array(arr_)


#################################
# Find the Nessies

nessie = """\
                  # 
#    ##    ##    ###
 #  #  #  #  #  #   """
nessie_length = len(nessie.split('\n')[0])


def get_shifted_nessie_exp(n_: int, nessie_: str) -> re.Pattern:
    nessie_ = nessie_.replace(' ', '.')
    shift = f'^.{{{n_}}}'
    exp = '.*' + '\n'.join([shift + row_ + '.*$' for row_ in nessie_.split('\n')])
    # exp = '\n'.join([shift + row_ for row_ in nessie_.split('\n')])
    return re.compile(exp, re.MULTILINE)


count = dict(zip(range(8), [0]*8))
for i, p in enumerate(pic_orientations(pic)):
    for sh_ in range(len(p.split('\n')[0]) - nessie_length):
        ns = get_shifted_nessie_exp(sh_, nessie).findall(p)
        if ns:
            # print('\n'.join(ns))
            # print()
            count[i] += len(ns)
nessies = [n_ for n_ in count.values() if n_ != 0]
assert len(nessies) == 1
nessies = nessies[0]
print('Part B:')
print(f'  Nessies: {nessies}')
print(f'  Water roughness: {pic.count("#") - count[2]*nessie.count("#")}')
