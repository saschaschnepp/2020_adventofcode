from typing import List, Tuple
from copy import deepcopy
from functools import lru_cache

with open('input.txt') as f:
    raw = f.read().split('\n\n')

p1 = list(map(int, raw[0].split('\n')[1:]))
p2 = list(map(int, raw[1].split('\n')[1:]))


def game(p1_: Tuple[int], p2_: Tuple[int]) -> Tuple[int, Tuple[int], Tuple[int]]:

    def game_winner(p1_, p2_):
        if p1_:
            return 1
        else:
            return 2

    while p1_ and p2_:
        l1, l2 = list(p1_), list(p2_)
        c1, c2 = l1.pop(0), l2.pop(0)
        p1_, p2_ = tuple(l1), tuple(l2)
        if c1 > c2:
            p1_ = tuple(list(l1) + [c1, c2])
        else:
            p2_ = tuple(list(l2) + [c2, c1])
        # print(p1_, p2_)
    return game_winner(p1_, p2_), p1_, p2_


@lru_cache(None)
def recursive_game(p1_: Tuple[int], p2_: Tuple[int]) -> Tuple[int, Tuple[int], Tuple[int]]:

    cache = set()

    def game_winner(p1_, p2_):
        if p1_:
            return 1
        else:
            return 2

    while p1_ and p2_:
        if (p1_, p2_) in cache:
            return 1, p1_, p2_
        cache.add((p1_, p2_))

        l1, l2 = list(p1_), list(p2_)
        c1, c2 = l1.pop(0), l2.pop(0)
        p1_, p2_ = tuple(l1), tuple(l2)
        if c1 <= len(p1_) and c2 <= len(p2_):
            winner_, _, _ = recursive_game(deepcopy(p1_[:c1]), deepcopy(p2_[:c2]))
            if winner_ == 1:
                p1_ = tuple(list(l1) + [c1, c2])
            else:
                p2_ = tuple(list(l2) + [c2, c1])
        elif c1 > c2:
            p1_ = tuple(list(l1) + [c1, c2])
        else:
            p2_ = tuple(list(l2) + [c2, c1])
        # print(p1_, p2_)
    return game_winner(p1_, p2_), p1_, p2_


def calc_result(p1_, p2_):

    if winner == 1:
        p = p1_
    else:
        p = p2_
    accu = 0
    for i, c in enumerate(reversed(p)):
        accu += (i + 1) * c
    return accu


winner, p1_, p2_ = game(tuple(p1), tuple(p2))
accu = calc_result(p1_, p2_)
print(f'Winner: {winner}, Result: {accu}')

winner, p1_, p2_ = recursive_game(tuple(p1), tuple(p2))
accu = calc_result(p1_, p2_)
print(f'Winner: {winner}, Result: {accu}')
