import numpy as np
from functools import reduce, lru_cache
from operator import add

with open('input.txt') as f:
    raw = list(map(int, f.read().split('\n')))


inp = [0] + raw + [max(raw)+3]


def part_a(inp_):
    inp_ = np.asarray(sorted(inp_))
    diff = inp_[1:] - inp_[:-1]
    ones = len([x for x in diff if x == 1])
    tres = len([x for x in diff if x == 3])
    print(ones * tres)


@lru_cache(maxsize=len(inp))
def part_b_rec(arr):
    if len(arr) == 1:
        return 1
    else:
        opts = []
        arr = sorted(list(arr))
        for i, _ in enumerate([x for x in arr[1:4] if x <= arr[0]+3]):
            opts.append(frozenset(arr[i+1:]))

        return reduce(add, map(part_b_rec, opts))


part_a(inp)
print(part_b_rec(frozenset(inp)))

