from copy import deepcopy

with open('input.txt') as f:
    instructions = [[z.split(" ")[0], int(z.split(" ")[1])] for z in f.read().split('\n')]


def swap(op_):
    if op_ == 'nop':
        return 'jmp'
    elif op_ == 'jmp':
        return 'nop'
    else:
        raise Exception('Invalid op')


def eval_instr(instr, idx_, acc_):
    if instr[0] == 'nop':
        idx_ += 1
    elif instr[0] == 'acc':
        idx_ += 1
        acc_ += instr[1]
    elif instr[0] == 'jmp':
        idx_ += instr[1]
    else:
        raise Exception("Invalid command")

    return idx_, acc_


def try_set(instructions_):
    accumulator = 0
    idx = 0
    visited = {0}

    while True:
        idx, accumulator = eval_instr(instructions_[idx], idx, accumulator)
        if idx in visited:
            # print(accumulator)
            return False, accumulator
        elif idx == len(instructions_):
            return True, accumulator
        visited.add(idx)


i = 0
while i < len(instructions):
    instructions_ = deepcopy(instructions)
    while instructions_[i][0] == 'acc':
        i += 1
    instructions_[i][0] = swap(instructions_[i][0])
    success, accumulator = try_set(instructions_)
    if success:
        print(accumulator)
        break
    i += 1
