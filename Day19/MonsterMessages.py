import re
from itertools import product
from functools import lru_cache
from typing import List

with open('input.txt') as f:
    rules, messages = f.read().split('\n\n')
rules = dict(map(lambda x: x.split(': '), rules.split('\n')))
messages = messages.split('\n')
rules_eval = dict(zip(rules.keys(), []*len(rules)))


def cross_product(rules_: List[List[str]]) -> List[str]:
    if len(rules_) == 1:
        return rules_[0]

    if len(rules_) == 2:
        prods = product(*rules_)
    else:
        prods = product(rules_[0], cross_product(rules_[1:]))
    rls = [''.join(p) for p in prods]
    return rls


@lru_cache(None)
def eval_rule(key_: str):
    rule_: str = rules[key_]
    if rule_.startswith('"'):
        rules_eval[key_] = [rule_.replace('"', '')]
        return rules_eval[key_]
    else:
        terms = []
        for term_ in rule_.split(' | '):
            parts = [eval_rule(r) for r in term_.split()]
            rls = cross_product(parts)
            for r in rls:
                terms.append(r)
        rules_eval[key_] = terms
        return terms


eval_rule('0')

# A
exp42_ = '(' + '|'.join([f'({r})' for r in rules_eval['42']]) + ')'
exp31_ = '(' + '|'.join([f'({r})' for r in rules_eval['31']]) + ')'
exp_a = re.compile(f'^{exp42_}{exp42_}{exp31_}$')

matches = []
for m in messages:
    if exp_a.match(m):
        matches.append(m)
print(f'Part A: {len(matches)}')

# B
exp_b_begin = f'^{exp42_}' + r'{2,}'
rexp_b_begin = re.compile(exp_b_begin)
exp_b_end = f'{exp31_}' + r'+$'
rexp_b_end = re.compile(f'({exp_b_end})')
rexp_b = re.compile(f'{exp_b_begin}{exp_b_end}')

matches = []
for m in messages:
    # People who are better than me with regular expressions will find a nicer way to check
    # that there are more sequences of rule 42 than 31
    if rexp_b.match(m) and len(rexp_b_begin.search(m)[0]) > len(rexp_b_end.search(m)[0]):
        matches.append(m)
print(f'Part B: {len(matches)}')
