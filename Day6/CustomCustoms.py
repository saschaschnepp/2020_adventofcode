from functools import reduce
from operator import add

with open('input.txt') as f:
    raw = f.read().split('\n\n')

print(reduce(add, map(len, map(set, map(lambda x: x.replace('\n', ''), raw)))))

with open('input.txt') as f:
    raw = f.read().split('\n\n')

total = 0
for r in raw:
    answers = r.split("\n")
    sets = [set(s) for s in answers]
    total += len(reduce(set.intersection, sets))
print(total)



