import re
import numpy as np
from functools import reduce
from operator import add, mul, itemgetter

with open('input.txt') as f:
    raw = f.read().split('\n\n')

data = dict(zip(['rules', 'my', 'others'], raw))
data['rules'] = data['rules'].split('\n')
data['my'] = data['my'].split('\n')[1]
data['others'] = data['others'].split('\n')[1:]

pos_field = {}


def part_a():
    exp = re.compile(r'\d+-\d+')
    valid = set()
    for vals in data['rules']:
        ranges = exp.findall(vals)
        for r in ranges:
            strt, till = list(map(int, r.split('-')))
            valid.update(range(strt, till+1))

    invalid = []
    for ticket in data['others']:
        # invalid.update(set(map(int, ticket.split(','))).difference(valid))
        invalid += [x for x in map(int, ticket.split(',')) if x not in valid]
    print(reduce(add, invalid))


def assign_position(dct: dict):
    global pos_field  # gaahhhh! But I'm too lazy to pass it in as an argument
    if not dct:
        return
    unique = {f: v[0] for f, v in dct.items() if len(v) == 1}
    pos_field = {**pos_field, **unique}
    for p, v in dct.items():
        dct[p] = [vv for vv in v if vv not in unique.values()]
    assign_position({p: v for p, v in dct.items() if p not in unique})


def part_b():

    # exclude invalid tickets
    exp = re.compile(r'\d+-\d+')
    valid = set()
    for vals in data['rules']:
        ranges = exp.findall(vals)
        for r in ranges:
            strt, till = list(map(int, r.split('-')))
            valid.update(range(strt, till+1))

    invalid = []
    for ticket in data['others']:
        # invalid.update(set(map(int, ticket.split(','))).difference(valid))
        if [x for x in map(int, ticket.split(',')) if x not in valid]:
            invalid.append(ticket)
    data['others'] = [t for t in data['others'] if t not in invalid]

    # find field ranges
    exp = re.compile(r'\d+-\d+')
    field_ranges = {}
    for vals in data['rules']:
        valid = set()
        ranges = exp.findall(vals)
        for r in ranges:
            strt, till = list(map(int, r.split('-')))
            valid.update(range(strt, till+1))
        field = vals.split(':')[0]
        field_ranges[field] = valid

    # find field numbers
    values = []
    for t in data['others']:
        values.append(list(map(int, t.split(','))))
    df = np.array(values)
    feasible = {f: [] for f in field_ranges.keys()}
    for col in range(df.shape[1]):
        ticket_vals = set(df[:, col])
        for f, v in field_ranges.items():
            if not ticket_vals - v:
                feasible[f].append(col)
    assign_position(feasible)
    print(reduce(mul,
                 map(
                     lambda i: list(map(int, data['my'].split(',')))[i],
                     map(
                         itemgetter(1),
                         filter(lambda x: x[0].startswith('departure'), pos_field.items())
                     )
                 )
                 )
          )


if __name__ == '__main__':
    part_a()
    part_b()
