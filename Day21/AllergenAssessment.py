import re
from collections import defaultdict, Counter
from functools import reduce
from operator import add
from pprint import pprint
from typing import Dict, Set

with open('input.txt') as f:
    raw = f.read().split('\n')

counter = Counter()
foods = defaultdict(list)
exp = re.compile(r'^((\w+ )+)\(contains ((\w)+(, \w+)*)\)$')
for r_ in raw:
    m_ = exp.match(r_)
    ings_ = set(m_.group(1).strip().split(' '))
    alls_ = frozenset(m_.group(3).split(', '))
    counter.update(ings_)
    for a_ in alls_:
        foods[a_].append(ings_)


allergens_to_ingredients = defaultdict(set)
for a_, f_ in foods.items():
    allergens_to_ingredients[a_] = set.intersection(*f_)


def solve(candidates_: Dict[str, Set[str]]):
    if reduce(max, map(len, candidates_.values())) == 1:
        return candidates_

    assigned_ = {a: i for a, i in candidates_.items() if len(i) == 1}
    for a, i in candidates_.items():
        if a in assigned_:
            continue
        solved = reduce(lambda x, y: x.union(y), assigned_.values())
        candidates_[a] -= solved
    return solve(candidates_)


allergens_to_ingredients = solve(allergens_to_ingredients)
pprint(allergens_to_ingredients)
ingredients_with_allergens = set.union(*allergens_to_ingredients.values())
part_a = reduce(add, [v for k, v in counter.items() if k not in ingredients_with_allergens])
print(f'Part A: {part_a}')

list_b = [next(iter(allergens_to_ingredients[a])) for a in sorted(allergens_to_ingredients)]
part_b = ','.join(list_b)
print(f'Part B: {part_b}')
