with open('input.txt') as f:
    raw = f.read().split('\n')


def make_bin(i: int):
    return [b for b in format(i, '036b')]


def make_int(b: list):
    return int(''.join(b), 2)


class PartA:

    def __init__(self, raw_):
        self.raw = raw_
        self.mask = {}
        self.mem = {}

    def process_value(self, num_: int):
        b = make_bin(num_)
        for k, v in self.mask.items():
            b[k] = v
        return make_int(b)

    def process_row(self, r: str):
        if r.startswith('mask'):
            self.mask = {k: v for k, v in enumerate(r.split(' = ')[-1]) if v != 'X'}
        else:
            pos = int(r.split(' = ')[0][4:-1])
            self.mem[pos] = self.process_value(int(r.split(' = ')[-1]))

    def process(self):
        for r in self.raw:
            self.process_row(r.strip())

    def sum_values(self):
        # print(self.mem)
        print(sum(self.mem.values()))


class PartB:

    def __init__(self, raw_):
        self.raw = raw_
        self.mask = {}
        self.mem = {}

    def apply_mask(self, b_pos: list):
        for i, v in enumerate(self.mask):
            if v in ['1', 'X']:
                b_pos[i] = v
        return b_pos

    def get_all_pos(self, b_pos):
        if not len(b_pos):
            return [[]]
        elif b_pos[0] != 'X':
            rec = self.get_all_pos(b_pos[1:])
            return [[b_pos[0]] + r for r in rec]
        else:
            rec = self.get_all_pos(b_pos[1:])
            return [['0'] + r for r in rec] + [['1'] + r for r in rec]

    def write_value(self, val, pos):
        b_pos = make_bin(pos)
        b_pos = self.apply_mask(b_pos)
        all_pos = self.get_all_pos(b_pos)
        for pos in all_pos:
            p = make_int(pos)
            self.mem[p] = val

    def process_row(self, r: str):
        if r.startswith('mask'):
            self.mask = [b_ for b_ in r.split(' = ')[-1]]
        else:
            pos = int(r.split(' = ')[0][4:-1])
            self.write_value(int(r.split(' = ')[-1]), pos)

    def process(self):
        for r in self.raw:
            self.process_row(r.strip())

    def sum_values(self):
        # print(self.mem)
        print(sum(self.mem.values()))


if __name__ == '__main__':
    a = PartA(raw)
    a.process()
    a.sum_values()

    b = PartB(raw)
    b.process()
    b.sum_values()
